﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SuperHeros.DATA;

namespace SuperHeros.UI.Controllers {
	//[Authorize(Roles = "Admin")]
	public class SuperPeoplesController : Controller {
		private HerosEntities db = new HerosEntities();

		[Authorize(Roles = "Admin, Hero")]
		// GET: SuperPeoples
		public ActionResult Index() {
			var superPeoples = db.SuperPeoples.Include(s => s.Alignment);
			return View(superPeoples.ToList());
		}
		[Authorize(Roles = "Admin, Hero")]
		// GET: SuperPeoples/Details/5
		public ActionResult Details(int? id) {
			if (id == null) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			SuperPeople superPeople = db.SuperPeoples.Find(id);
			if (superPeople == null) {
				return HttpNotFound();
			}
			return View(superPeople);
		}
		[Authorize(Roles = "Admin")]
		// GET: SuperPeoples/Create
		public ActionResult Create() {
			ViewBag.AlignmentID = new SelectList(db.Alignments, "AlignmentID", "Alignment1");
			return View();
		}
		// POST: SuperPeoples/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[Authorize(Roles = "Admin")]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "SuperPersonID,FirstName,LastName,Alias,Origin,AlignmentID")] SuperPeople superPeople) {
			if (ModelState.IsValid) {
				db.SuperPeoples.Add(superPeople);
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			ViewBag.AlignmentID = new SelectList(db.Alignments, "AlignmentID", "Alignment1", superPeople.AlignmentID);
			return View(superPeople);
		}
		[Authorize(Roles = "Admin")]
		// GET: SuperPeoples/Edit/5
		public ActionResult Edit(int? id) {
			if (id == null) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			SuperPeople superPeople = db.SuperPeoples.Find(id);
			if (superPeople == null) {
				return HttpNotFound();
			}
			ViewBag.AlignmentID = new SelectList(db.Alignments, "AlignmentID", "Alignment1", superPeople.AlignmentID);
			return View(superPeople);
		}
		// POST: SuperPeoples/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = "Admin")]
		public ActionResult Edit([Bind(Include = "SuperPersonID,FirstName,LastName,Alias,Origin,AlignmentID")] SuperPeople superPeople) {
			if (ModelState.IsValid) {
				db.Entry(superPeople).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			ViewBag.AlignmentID = new SelectList(db.Alignments, "AlignmentID", "Alignment1", superPeople.AlignmentID);
			return View(superPeople);
		}
		[Authorize(Roles = "Admin")]
		// GET: SuperPeoples/Delete/5
		public ActionResult Delete(int? id) {
			if (id == null) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			SuperPeople superPeople = db.SuperPeoples.Find(id);
			if (superPeople == null) {
				return HttpNotFound();
			}
			return View(superPeople);
		}
		// POST: SuperPeoples/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = "Admin")]
		public ActionResult DeleteConfirmed(int id) {
			SuperPeople superPeople = db.SuperPeoples.Find(id);
			db.SuperPeoples.Remove(superPeople);
			db.SaveChanges();
			return RedirectToAction("Index");
		}
		protected override void Dispose(bool disposing) {
			if (disposing) {
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
