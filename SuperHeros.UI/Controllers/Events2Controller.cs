﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SuperHeros.DATA;

namespace SuperHeros.UI.Controllers {
	[Authorize(Roles = "Admin")]
	public class Events2Controller : Controller {
		private HerosEntities db = new HerosEntities();
		[AllowAnonymous]
		// GET: Events2
		public ActionResult Index() {
			return View(db.Events2.ToList());
		}
		[AllowAnonymous]
		// GET: Events2/Details/5
		public ActionResult Details(int? id) {
			if (id == null) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Events2 events2 = db.Events2.Find(id);
			if (events2 == null) {
				return HttpNotFound();
			}
			return View(events2);
		}

		// GET: Events2/Create
		public ActionResult Create() {
			return View();
		}

		// POST: Events2/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "EventID,EventName,Instructor,EventDate,EventTime,EventDescription")] Events2 events2) {
			if (ModelState.IsValid) {
				db.Events2.Add(events2);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			return View(events2);
		}

		// GET: Events2/Edit/5
		public ActionResult Edit(int? id) {
			if (id == null) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Events2 events2 = db.Events2.Find(id);
			if (events2 == null) {
				return HttpNotFound();
			}
			return View(events2);
		}

		// POST: Events2/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "EventID,EventName,Instructor,EventDate,EventTime,EventDescription")] Events2 events2) {
			if (ModelState.IsValid) {
				db.Entry(events2).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			return View(events2);
		}

		// GET: Events2/Delete/5
		public ActionResult Delete(int? id) {
			if (id == null) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Events2 events2 = db.Events2.Find(id);
			if (events2 == null) {
				return HttpNotFound();
			}
			return View(events2);
		}

		// POST: Events2/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id) {
			Events2 events2 = db.Events2.Find(id);
			db.Events2.Remove(events2);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing) {
			if (disposing) {
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
